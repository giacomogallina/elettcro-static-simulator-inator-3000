#!/usr/bin/python

import numpy as np
from numpy.linalg import norm
import copy
import types
from stl import mesh
from matplotlib import pyplot
from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d.art3d import Poly3DCollection, Line3DCollection


def grad(v, f):
    d = 1e-6
    f_v = f(v)
    return np.array([
        f(v + [d, 0, 0]) - f_v,
        f(v + [0, d, 0]) - f_v,
        f(v + [0, 0, d]) - f_v
        ]) / d


def adjust(v, f):
    eps = 0.000000000001
    f_v = f(v)
    while np.dot(f_v, f_v) > eps:
        g_v = grad(v, f)
        v -= f_v / np.dot(g_v, g_v) * g_v
        f_v = f(v)
    return v


def tassellate(f, box, step):
    dirs = [
            np.array([1.0, 0.0, 0.0]),
            np.array([0.0, 1.0, 0.0]),
            np.array([0.0, 0.0, 1.0]),
            np.array([-1.0, 0.0, 0.0]),
            np.array([0.0, -1.0, 0.0]),
            np.array([0.0, 0.0, -1.0])
            ]
    faces = []
    for d in dirs:
        z = list(i for i in range(3) if d[i] == 0)
        face = []
        for i in range(4):
            face.append(copy.deepcopy(d))
            face[i][z[0]] = (-1)**(i in [1, 2])
            face[i][z[1]] = (-1)**(i//2)
        faces.append(face)

    squares = []
    for x in np.arange(box[0][0], box[0][1], step):
        for y in np.arange(box[1][0], box[1][1], step):
            for z in np.arange(box[2][0], box[2][1], step):
                v = np.array([x, y, z])
                if f(v) < 0: 
                    for i in range(6):
                        d = dirs[i]
                        if f(v + step * d) >= 0:
                            squares.append(list(v + step * 0.5 * p for p in faces[i]))
    shape = Shape(squares)
    shape.adjust(f)
    return shape


def area(face):
    return 0.5 * norm(np.outer(face[0] - face[2], face[1] - face[3]))


def center(face):
    return 0.25 * sum(face)


class Shape:
    faces = []

    def __init__(self, faces):
        self.faces = faces

    def linear_transform(self, mat):
        for face in self.faces:
            for i in range(len(face)):
                face[i] = mat.dot(face[i])

    def move(self, d):
        for face in self.faces:
            for vert in face:
                vert += d

    def linear_transform_around(self, mat, center):
        self.move(-center)
        self.linear_transform(mat)
        self.move(center)

    def subdivide(self):
        new_faces = []
        for face in self.faces:
            c = center(face)
            for i in range(4):
                new_faces.append([face[i], 0.5 * (face[i] + face[(i+1)%4]), c, 0.5 * (face[i] + face[(i+3)%4])])
        self.faces = new_faces

    def adjust(self, f):
        for face in self.faces:
            for p in face:
                p = adjust(p, f)

    def save(self):
        m = mesh.Mesh(np.zeros(2 * len(self.faces), dtype=mesh.Mesh.dtype))
        for i in range(len(self.faces)):
            m.vectors[2*i][0] = self.faces[i][0]
            m.vectors[2*i][1] = self.faces[i][1]
            m.vectors[2*i][2] = self.faces[i][2]
            m.vectors[2*i+1][0] = self.faces[i][2]
            m.vectors[2*i+1][1] = self.faces[i][3]
            m.vectors[2*i+1][2] = self.faces[i][0]

        m.save("shape.stl")


class Scene:
    shapes = []
    shape_charges = []
    point_charges = []
    point_charges_pos = []
    potentials = []
    charge_dist = None
    
    def add(self, thing, charge = 0.0):
        if type(thing) is Shape:
            self.shapes.append(thing)
            self.shape_charges.append(charge)
        elif type(thing) is types.FunctionType:
            self.potentials.append(thing)
        else:
            if type(thing) is list:
                thing = np.array(thing)
            self.point_charges_pos.append(thing)
            self.point_charges.append(charge)
        self.charge_dist = None

    def clear(self):
        self.shapes.clear()
        self.shape_charges.clear()
        self.point_charges.clear()
        self.point_charges_pos.clear()
        self.potentials.clear()
        self.charge_dist = None

    def solve(self):
        if self.charge_dist == None:
            n = sum(len(shape.faces) for shape in self.shapes)
            l = len(self.shapes)
            c = 1 if len(self.point_charges) + len(self.potentials) > 0 else 0
            print(f"solving scene with {l} objects,",
                    f"{len(self.point_charges)} point charges,",
                    f"{len(self.potentials)} potentials, {n} total faces")
            A = np.eye(n+c, n+c)
            faces = sum((shape.faces for shape in self.shapes), start = [])
            p = np.array(list(center(face) for face in faces))

            for i in range(3):
                A[0:n, 0:n] += (np.reshape(p[:, i], (n, 1)).dot(np.ones((1, n))) - np.ones((n, 1)).dot(np.reshape(p[:, i], (1, n))))**2
            A[0:n, 0:n] = 1.0 / A[0:n, 0:n]**0.5

            for (q, pos) in zip(self.point_charges, self.point_charges_pos):
                v = q / np.sum((p - pos)**2, axis = 1)**0.5
                A[n, 0:n] += v
                A[0:n, n] += v

            for pot in self.potentials:
                for i in range(n):
                    A[n, i] += pot(p[i])
                    A[i, n] += pot(p[i])

            b = np.zeros(n+2*c+l)
            i = 0
            A = np.block([[A, np.zeros((n+c, l+c))], [np.zeros((l+c, n+2*c+l))]])
            for s in range(l):
                for face in self.shapes[s].faces:
                    A[i, i] = (2.98 / area(face)**0.5)
                    A[n+c+s, i] = A[i, n+c+s] = 1.0
                    i += 1
                b[n+c+s] = self.shape_charges[s]

            if c > 0:
                A[n, n] = 0.0
                A[n+c+l, n] = A[n, n+c+l] = 1.0
                b[n+c+l] = 1

            q = np.linalg.solve(A, b)
            self.charge_dist = q[0:n]

    def plot(self):
        if len(self.shapes) + len(self.point_charges) == 0:
            print("there's nothing to plot")
            return
        m = []    
        for shape in self.shapes:
            faces = shape.faces
            for i in range(len(faces)):
                m.append([faces[i][2], faces[i][3], faces[i][0]])
                m.append([faces[i][0], faces[i][1], faces[i][2]])

        fig = pyplot.figure()
        ax = fig.add_subplot(111, projection='3d')

        scale = np.block([np.array(m).flatten(), np.array(self.point_charges_pos).flatten()])
        if self.charge_dist is None:
            d = Poly3DCollection(m, linewidths=1)
        else:
            sigma = []
            i = 0
            for shape in self.shapes:
                sigma += list(qi/area(Ai) for (qi, Ai) in zip(self.charge_dist[i:], shape.faces))
                i += len(shape.faces)
            maxs = max(abs(i) for i in sigma)
            colors = list((1-max(0, -i/maxs), 1-abs(i/maxs), 1-max(0, i/maxs)) for i in sigma)
            colors = sum(([a, b] for (a, b) in zip(colors, colors)), start = [])

            d = Poly3DCollection(m, facecolors = colors, linewidths=1)

        ax.add_collection3d(d)

        for (v, q) in zip(self.point_charges_pos, self.point_charges):
            ax.scatter([v[0]], [v[1]], [v[2]], color = (q > 0, 0, q < 0))

        ax.auto_scale_xyz(scale, scale, scale)
        pyplot.show()


def ellipsoid(a, b, c, center = np.array([0.0, 0.0, 0.0])):
    if type(center) == list:
        center = np.array(center)
    s = np.array([1/a**2, 1/b**2, 1/c**2])
    f = lambda v: (v - center).dot((v - center) * s) - 1
    box = [
            [center[0] - 1.1*a, center[0] + 1.1*a], 
            [center[1] - 1.1*b, center[1] + 1.1*b], 
            [center[2] - 1.1*c, center[2] + 1.1*c]
            ]
    return tassellate(f, box, min(a, b, c) / 5)


def sphere(r = 1.0, center = np.array([0.0, 0.0, 0.0])):
    return ellipsoid(r, r, r, center)


def thorus(R = 2.0, r = 1.0, center = np.array([0.0, 0.0, 0.0])):
    if type(center) == list:
        center = np.array(center)
    f = lambda v: (((v[0] - center[0])**2 + (v[1] - center[1])**2)**0.5 - R)**2 + (v[2] - center[2])**2 - r**2
    box = [
            [center[0] - 1.1*(R+r), center[0] + 1.1*(R+r)], 
            [center[1] - 1.1*(R+r), center[1] + 1.1*(R+r)], 
            [center[2] - 1.1*r, center[2] + 1.1*r]
            ]
    return tassellate(f, box, r / 4)


def pear():
    f = lambda v: (v[0]**2 + (v[1]**2 + v[2]**2))*(1 + 2*v[0] + 5*v[0]**2 + 6*v[0]**3 + 6*v[0]**4 + 4*v[0]**5 + v[0]**6 - 3*(v[1]**2 + v[2]**2) -2*v[0]*(v[1]**2 + v[2]**2) + 8*v[0]**2*(v[1]**2 + v[2]**2) + 8*v[0]**3*(v[1]**2 + v[2]**2) + 3*v[0]**4*(v[1]**2 + v[2]**2) + 2*(v[1]**2 + v[2]**2)**2 + 4*v[0]*(v[1]**2 + v[2]**2)**2 + 3*v[0]**2*(v[1]**2 + v[2]**2)**2 + (v[1]**2 + v[2]**2)**3) - 4
    return tassellate(f, [[-3, 3], [-3, 3], [-3, 3]], 0.1)

if __name__ == "__main__":
    scene = Scene()
    # scene.add(sphere(1), 1.0)
    # scene.add(sphere(1, np.array([2.5, 0, 0])), 0.0)
    # scene.add(ellipsoid(1.5, 2, 1, np.array([0.0, 0.0, 3.0])), 0.0)
    # donut = thorus(1.5, 1, np.array([0.0, 0.0, -2.0]))
    # donut.move(np.array([0.0, 0.0, -0.5]))
    # donut.linear_transform(np.array([[0.0, 0.0, 1.0], [0.0, 1.0, 0.0], [1.0, 0.0, 0.0]]))
    # scene.add(donut, -2.0)
    # scene.plot()
    # scene.solve()
    # scene.plot()

    # scene.clear()
    scene.add(sphere())
    scene.add([2, 2, 2], 5.0)
    scene.add(lambda v: v[0])
    scene.solve()
    scene.plot()
