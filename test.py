for n in [4, 8, 16, 32, 64, 128]:
    U = 0.0
    for a in range(n):
        for c in range(4*n):
            for d in range(c):
                U += 1.0 / (c-d)
        for b in range(a):
            for c in range(4*n):
                for d in range(4*n):
                    U += 1.0 / ((a-b)**2 + (c-d)**2)**0.5
    print(U / 16 / n**3)
