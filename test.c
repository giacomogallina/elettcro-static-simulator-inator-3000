#include <stdio.h>
#include <math.h>

int main() {
		double U = 0.0;

		int n = 1024;

		for (int a = 0; a < n; a++) {
			printf("a = %d\n", a);
			for (int c = 0; c < n; c++) {
				for (int d = 0; d < c; d++) {
						U += 1.0 / (c - d);
				}
			}
			for (int b = 0; b < a; b++) {
				for (int c = 0; c < n; c++) {
					for (int d = 0; d < n; d++) {
							U += 1.0 / sqrt((a-b)*(a-b) + (c-d)*(c-d));
					}
				}
			}
		}
		printf("%lf\n", U / (n*n*n));
}
